# mumble registration server written in python3
from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.parse
import mysql.connector
import re

def addusermysql(username, password):
    cnx = mysql.connector.connect(user='mumble', password='mysqlpassword', host='127.0.0.1', database='murmur')
    cursor = cnx.cursor()
    cursor.execute("SELECT * from users WHERE name=%(username)s",{ 'username': username })
    records = cursor.fetchall()
    if not records:
        cursor.execute("SELECT user_id FROM users ORDER BY user_id DESC LIMIT 0, 1")
        lastuserid = cursor.fetchall()
        data = (1, lastuserid[0][0]+1, username, password)
        print(data)
        cursor.execute("INSERT INTO users (server_id, user_id, name, pw) VALUES (%s,%s,%s,%s)", data)
    else:
        cursor.execute("UPDATE users SET pw=%(password)s WHERE name=%(username)s", { 'password': password, 'username': username })
    cnx.commit()
    cnx.close()

def validusername(nick):
    if not nick:
        print("String empty")
        return False
    elif re.search('[^a-zA-Z0-9\-\[\]\'`^{}_]', nick):
        print("nickname not valid")
        return False
    elif nick == "SuperUser":
        return False
    else:
        return True

def validpassword(password):
    print(len(password))
    if not password:
        print("String empty")
        return False
    elif len(password) != 40:
        print("Password not valid")
        return False
    elif re.search('[^a-fA-F0-9]', password):
        print("password not valid")
        return False
    else:
        return True




class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        username = ""
        password = ""
        mastpasswordclient = ""
        request = urllib.parse.urlparse(self.path)
        requestvals = request.query.split("&")
        masterpassword = "thisneedstobelongandrandom"

        for val in requestvals:
            if "username=" in val:
                username = val[val.find("=")+1:].strip()
            if "password=" in val:
                password = val[val.find("=")+1:].strip()
            if "serverpassd=" in val:
               mastpasswordclient = val[val.find("=")+1:].strip()

        self.send_response(200)
        self.end_headers()
        if(mastpasswordclient == masterpassword and validusername(username) and validpassword(password)):
            addusermysql(username, password)
            self.wfile.write(b'User Registered')
        else:
            self.wfile.write(b'Authentication Failed')

httpd = HTTPServer(('localhost', 64740), SimpleHTTPRequestHandler)
httpd.serve_forever()

