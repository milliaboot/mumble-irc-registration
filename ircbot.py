#!/usr/bin/env python
"""
IRC Bot for registering IRC users with mumble's authentication backend
"""
from os import getenv
from sys import exit
import socket
import ssl
import time
# import re
import string
import random
import urllib.request
import hashlib
import signal
from socket import timeout
#includes for mumble
import pymumble.pymumble_py3 as pymumble_py3


# irc server config
host="irc.darkscience.net"
port = 6697
nick = "mumblebot"
password = "nickpassword"
channel = "#voicechat"
registationlocation = "/registering"
masterpassword = getenv('BOT_PASSWORD', "<wowbuddy>")
mumbleserveraddress = getenv('MUMBLE_SERVER', 'voip.drk.sc')
nickservstr = ":NickServ!NickServ@services.darkscience.net"
registeringusers = []
joined = False

#mumble configuration
mumblenick = "mumblebot"
mumblepassword = "<wowbuddy>"
mumblechannelname = "voicechat"


def getircdata(irc):
    try:
        ircdata = irc.recv(2048)
    except KeyboardInterrupt:
        exit(0)
    except:
        ircdata = "error getting irc data"
    return ircdata

def decodeircmessage(message):
    try:
        decodedmessages = message.decode("utf-8")
    except:
        decodedmessages = "error decoding irc data"
    return decodedmessages

def printmessage(message):
    try:
        print(message)
    except:
        print("Error Printing message")
    return

def countconnectedmumbleusers(mumble):
    numberofconnectedusers = 0
    for connectedmumbleuser in mumble.users:
        userid = mumble.users[connectedmumbleuser].get('user_id')
        if userid and userid > 5:
            numberofconnectedusers = numberofconnectedusers + 1
    return numberofconnectedusers

def pingpong(irc, text):
    if text.find('PING') != -1:
        sendstring = 'PONG ' + text.split()[1] + '\r\n'
        irc.send(bytes(sendstring, 'utf-8'))

def randompassword():
  chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
  size = random.randint(8, 12)
  return ''.join(random.choice(chars) for x in range(size))

def ConnectIRC():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as msg:
        print('Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1])
        exit(1)

    try:
        s.connect((host, port))
    except socket.error:
        print('Failed to connect to irc server')
        exit(1)
    #warning: only use for testing to allow for self signed certs. This will disable SSL verification
    #ctx = ssl.create_default_context()
    #ctx.check_hostname = False
    #ctx.verify_mode = ssl.CERT_NONE
    s.settimeout(300)
    irc = ssl.wrap_socket(s)
    #irc.send(bytes("PASS " + password + "\n", 'utf-8'))
    irc.send(bytes("USER {0} {0} {0} :Mumble Bot\n".format(nick), 'utf-8'))
    irc.send(bytes("NICK " + nick + "\n", 'utf-8'))
    while True:
        data = getircdata(irc)
        text = data.decode("utf-8")
        pingpong(irc, text)
        print(text)
        if not data:
            irc.close()
            exit(1)
        if text.find(':NickServ!NickServ@services.darkscience.net NOTICE ' + nick + ' :Welcome to Darkscience') != -1:
            break
    irc.send(bytes("MODE " + nick + " +B\r\n", 'utf-8'))  # DS requires this
    print("Connected to IRC server")
    return irc

def JoinChannel(irc, channelname):
    irc.send(bytes("JOIN " + channelname + "\r\n", 'utf-8'))


#connect to mumble server
mumble = pymumble_py3.Mumble(mumbleserveraddress, mumblenick, password=mumblepassword)
mumble.start()
time.sleep(15) # wait for bot to connect
mumblechannel = mumble.channels.find_by_name(mumblechannelname)
mumblechannel.move_in()

irc = ConnectIRC()
JoinChannel(irc, channel)
while True:
    data = getircdata(irc)
    if not data:
        irc.close()
        time.sleep(300) # delay before trying to reconnect to irc
        irc = ConnectIRC()
        JoinChannel(irc, channel)
        continue

    text = decodeircmessage(data)
    pingpong(irc, text)
    printmessage(text)

    if text.find(nick + ' ' + channel + ' :End of /NAMES list.') != -1:
        joined = True
    elif text.find('PRIVMSG ' + channel + ' :!register') != -1:
        split_text = text.split("!")
        user = split_text[0].strip(':')
        irc.send(bytes("PRIVMSG NickServ :ACC " + user + "\r\n", 'utf-8'))
        registeringusers.append(user.strip())

    elif text.find('PRIVMSG ' + channel + ' :!online') != -1:
        numberofconnectedusers = countconnectedmumbleusers(mumble)
        ircmessage = str(numberofconnectedusers) + " Online Users"
        irc.send(bytes("PRIVMSG " + channel + " :" + ircmessage + "\r\n", 'utf-8'))
    elif text.find('PRIVMSG ' + channel + ' :') != -1:
        messengernick = text[1:(text.find("!"))]
        messagetext = text.split('PRIVMSG ' + channel + ' :')[1].strip()
        compiledmessage = messengernick + ": " + messagetext
        mumblechannel.send_text_message(compiledmessage)

    for user in registeringusers:
        if text.find(nickservstr + " NOTICE " + nick + " :" + user + " ACC") != -1:
            accstring = text.split(":")[2].strip()
            if text.strip() == nickservstr + " NOTICE " + nick + " :" + user + " ACC 3":
                userpassword = randompassword()
                url = "https://" + mumbleserveraddress + registationlocation + "?username=" + user + "&password="+hashlib.sha1(userpassword.encode()).hexdigest() + "&serverpassd=" + masterpassword
                fp = urllib.request.urlopen(url, timeout=30)
                mybytes = fp.read()

                response = mybytes.decode("utf8")
                fp.close()

                if response == "User Registered":
                    irc.send(bytes("PRIVMSG " + user + " :User registered! Connect with standalone mumble client with Server: " + mumbleserveraddress + ", Username: " + user + ", Password: "+ userpassword + " Or connect with web mumble client (username and password autofilled): https://" + mumbleserveraddress + "/?username="+ user + "&password="+userpassword+"\r\n", 'utf-8'))
                else:
                    irc.send(bytes("PRIVMSG " + user + " :Error Adding User\r\n", 'utf-8'))
            else:
                irc.send(bytes("PRIVMSG " + user + " :You are not logged in\r\n", 'utf-8'))
            del registeringusers[registeringusers.index(user)]
